<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class chartcontroller extends Controller
{
    public function getData(){
    return view('chartsv2'); 
    }

    public function getDate(request $request){
        $month = $request->input('molohok');
        $filter = 201707;
        $dates = DB::table('ofuser')
            ->select(DB::raw("COUNT(*) AS value, to_char(to_timestamp(creationdate::float/1000),'yyyy-mm-dd')::date  AS datem"))
            ->whereRaw("to_char(to_timestamp(creationdate::float/1000),'yyyymm')::integer = ".$month)
            ->groupBy('datem')
            ->orderBy('datem', 'asc')
            ->get();

        $total = DB::table('ofuser')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->get()
        ->first();

        $categories = [];
        $dataSeries = [];

        foreach($dates as $molohok) {
            array_push($categories, $molohok->datem);
            array_push($dataSeries, (int) $molohok->value);
        };

        $result = response()->json([
            'categories' => $categories,
            'dataSeries' => $dataSeries,
            'total' => $total->palue
        ]);        
        return $result;
    }


    //speedTest
    public function getDateSpeed(request $request){
        $month = $request->input('molohoks');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('SpeedTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('SpeedTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohok) {
            array_push($categoriess, $molohok->datemm);
            array_push($dataSeriess, (int) $molohok->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }

    
    //VideoTest
    public function getDateVideo(request $request){
        $month = $request->input('molohokz');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('VideoTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('VideoTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohok) {
            array_push($categoriess, $molohok->datemm);
            array_push($dataSeriess, (int) $molohok->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }

    
    //WebTest
    public function getDateWeb(request $request){
        $month = $request->input('molohokk');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('WebTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('WebTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohokk) {
            array_push($categoriess, $molohokk->datemm);
            array_push($dataSeriess, (int) $molohokk->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }


    //BasicRouteTest
    public function getDateBasicTest(request $request){
        $month = $request->input('molohokko');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('RouteTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->where('mode', '=', 'B')
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('RouteTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->where('mode', '=', 'B')
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohokko) {
            array_push($categoriess, $molohokko->datemm);
            array_push($dataSeriess, (int) $molohokko->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }


    //DownloadRouteTest
    public function getDateDownloadTest(request $request){
        $month = $request->input('molohokko');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('RouteTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->where('mode', '=', 'D')
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('RouteTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->where('mode', '=', 'D')
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohokko) {
            array_push($categoriess, $molohokko->datemm);
            array_push($dataSeriess, (int) $molohokko->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }


    //UploadRouteTest
    public function getDateUploadTest(request $request){
        $month = $request->input('molohokko');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('RouteTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->where('mode', '=', 'U')
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('RouteTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->where('mode', '=', 'U')
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohokko) {
            array_push($categoriess, $molohokko->datemm);
            array_push($dataSeriess, (int) $molohokko->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }

    //VideoRouteTest
    public function getDateVideoTest(request $request){
        $month = $request->input('molohokko');
        // $filter = 201707;
        $dates = DB::connection('pgsql_sv')
            ->table('RouteTest')
            ->select(DB::raw("COUNT(*) AS value, to_char( time, 'yyyy-mm-dd') AS datemm"))
            ->whereRaw("to_char( time, 'yyyymm')::integer = ".$month)
            ->where('mode', '=', 'V')
            ->groupBy('datemm')
            ->orderBy('datemm', 'asc')
            ->get();

        $total = DB::connection('pgsql_sv')
        ->table('RouteTest')
        ->select(DB::raw("COUNT(*) AS palue"))
        ->where('mode', '=', 'V')
        ->get()
        ->first();

        $categoriess = [];
        $dataSeriess= [];

        foreach($dates as $molohokko) {
            array_push($categoriess, $molohokko->datemm);
            array_push($dataSeriess, (int) $molohokko->value);
        };

        $result = response()->json([
            'categories' => $categoriess,
            'dataSeries' => $dataSeriess,
            'total' => $total->palue
        ]);        
        return $result;
    }
}
