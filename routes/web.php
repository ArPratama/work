<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/index', 'koneksicontroller@index');

Route::get('/charts', function(){
    return view('charts');
});

Route::get('/chartsv2', 'chartcontroller@getData');

Route::get('/chartsGetDate', 'chartcontroller@getDate');

Route::get('/chartsGetSpeed', 'chartcontroller@getDateSpeed');

Route::get('/chartsGetVideo', 'chartcontroller@getDateVideo');

Route::get('/chartsGetWeb', 'chartcontroller@getDateWeb');
Route::get('/chartsGetBasic', 'chartcontroller@getDateBasicTest');
Route::get('/chartsGetDownload', 'chartcontroller@getDateDownloadTest');
Route::get('/chartsGetUpload', 'chartcontroller@getDateUploadTest');
Route::get('/chartsGetVideos', 'chartcontroller@getDateVideoTest');

Route::get('/chartsv2input', function(){
    return view('chartsv2input');
});