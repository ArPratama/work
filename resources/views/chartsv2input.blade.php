<html>
@extends('layouts.navbar')
@section('content')
<head>
<title>Form Input</title>
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('css/index.css')}}"/>

        <!-- Scripts -->
        <script src={{asset("js/jquery-3.0.0.min.js")}}></script>
        <script src={{asset("js/tether-1.2.0.min.js")}}></script>
        <script src={{asset("js/bootstrap.min.js")}}></script>
        <script src={{asset("js/bootstrap-select.min.js")}}></script>

<script>
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd = '0'+dd
} 

if(mm<10) {
    mm = '0'+mm
} 

today = mm + '/' + dd + '/' + yyyy;
</script>

</head>
<body>
    <form method="post">
<p><center><div class="titleMain">Input Daily Report</div></center></p>
<p><center>Date: <input type="hidden" name="date" readonly value="<?php echo date("Y/m/d"); ?>"><?php echo date("Y/m/d"); ?></center></p>
<div class="layout">
<center><div class="titleSub">Twitter</div></center>
<div class="Content">
<table>
    <tr>
        <td>Followers<td>
        <td>:<td>
        <td><input type="text" name="followers" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>
<tr>
        <td>Comment<td>
        <td>:<td>
        <td><input type="text" name="comment" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>
<tr>
        <td>Mention<td>
        <td>:<td>
        <td><input type="text" name="mention" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>
</table>
</div>
<hr>

<center><div class="titleSub">Facebook</div></center>
<div class="Content">
<table>
    <tr>
        <td>Page Like<td>
        <td>:<td>
        <td><input type="text" name="pagelike" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>
<tr>
        <td>Comment<td>
        <td>:<td>
        <td><input type="text" name="commentfb" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>
</table>
</div>
<hr>

<center><div class="titleSub">Instagram</div></center>
<div class="Content">
<table>
    <tr>
        <td>Followers<td>
        <td>:<td>
        <td><input type="text" name="followersig" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>
<tr>
        <td>Comment<td>
        <td>:<td>
        <td><input type="text" name="commentig" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required><td>

</tr>



</table>
</div>
</div>
<br>
<button type="submit" style="float:right; margin-right:5%; width:100px;" class="btn btn-primary">Save</button>
<br>
</form>
</body>
</html>
@stop