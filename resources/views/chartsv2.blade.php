<html>
@extends('layouts.navbar')
@section('content')
<head>
<title>Charts V.2</title>
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
<link rel="stylesheet" href="{{asset('css/index.css')}}"/>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="http://code.highcharts.com/stock/highstock.js"></script>

        <!-- Scripts -->
        <script src={{asset("js/jquery-3.0.0.min.js")}}></script>
        <script src={{asset("js/tether-1.2.0.min.js")}}></script>
        <script src={{asset("js/bootstrap.min.js")}}></script>
        <script src={{asset("js/bootstrap-select.min.js")}}></script>



</head>
<body>
<!-- <div class="text-right">                           
    <select id="month_filter" name="month_filter" class="btn custom-select">
            <option value=01>January</option>
            <option value=02>February</option>
            <option value=03>March</option>
            <option value=04>Aapril</option>
            <option value=05>May</option>
            <option value=06>June</option>
            <option value=07>July</option>
            <option value=08>August</option>
            <option value=09>September</option>
            <option value=10>October</option>
            <option value=11>November</option>
            <option value=12>December</option>
    </select>
        <select id="year_filter" class="btn custom-select">
            <option value=1>2017</option>
            <option value=2>2018</option>
 </select>
    <div class="btn btn-default">Submit</div>
</div> -->

<div class="row">
    <div class="col-md-12">
    
<div class="title">Daily Reporting Signal Viewer</div>

        <div class="main">
    <div id="installer"></div>
    <div class="title">Tests</div>    
    <div id="installer2"></div>
    <div id="installer3"></div>
    <div id="installer4"></div>
    <div class="title">Route Tests</div>
    <div id="installer5"></div>
    <div id="installer6"></div>
    <div id="installer7"></div>
    <div id="installer8"></div>

        </div>
    </div>
</div>
</html>
</body>
<script>
    $(document).ready(function(){
       getInstaller(201709);
       getInstaller2(201709);
       getInstaller3(201709);
       getInstaller4(201709);
       getInstaller5(201709);
       getInstaller6(201709);
       getInstaller7(201709);
       getInstaller8(201709);
    });
function getMonth(){
    var TodayDate = new Date();
    var d = TodayDate.getDate();
    var m = TodayDate.getMonth()+1;

    document.write('hari sekarang adalah tanggal : ' + d + ' dan bulan :' + m);
}
function getInstaller(monthYear) {
    url = "{{ action('chartcontroller@getDate') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohok: monthYear
        },
        success: function(dataResult){
            renderInstaller(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller(data){
Highcharts.chart('installer', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color: '#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Installer APK September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c',
            backgroundColor:'#24637c',
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            lineColor: '#24637c',
            fillColor: '#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#24637c',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: '#24637c',
                              fillColor: '#24637c',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}

function getInstaller2(monthYears) {
    url = "{{ action('chartcontroller@getDateSpeed') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohoks: monthYears
        },
        success: function(dataResult){
            renderInstaller2(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller2(data){
Highcharts.chart('installer2', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color: '#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Speed Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c'
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            lineColor: '#24637c',
            fillColor: '#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#24637c',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}


function getInstaller3(monthYears) {
    url = "{{ action('chartcontroller@getDateVideo') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohokz: monthYears
        },
        success: function(dataResult){
            renderInstaller3(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller3(data){
Highcharts.chart('installer3', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color: '#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Video Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c',
            backgroundColor:'#24637c',
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            lineColor: '#24637c',
            fillColor: '#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#24637c',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}


function getInstaller4(monthYears) {
    url = "{{ action('chartcontroller@getDateWeb') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohokk: monthYears
        },
        success: function(dataResult){
            renderInstaller4(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller4(data){
Highcharts.chart('installer4', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color: '#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Web Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c'
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            linecolor:'#24637c',
            fillcolor:'#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#638199',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}



//data Basic Test

function getInstaller5(monthYears) {
    url = "{{ action('chartcontroller@getDateBasicTest') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohokko: monthYears
        },
        success: function(dataResult){
            renderInstaller5(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller5(data){
Highcharts.chart('installer5', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color:'#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Basic Route Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c'
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            linecolor:'#24637c',
            fillcolor:'#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#638199',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}



//data Download Test

function getInstaller6(monthYears) {
    url = "{{ action('chartcontroller@getDateDownloadTest') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohokko: monthYears
        },
        success: function(dataResult){
            renderInstaller6(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller6(data){
Highcharts.chart('installer6', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color:'#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Download Route Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c'
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            linecolor:'#24637c',
            fillcolor:'#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#638199',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}


//data Upload Test

function getInstaller7(monthYears) {
    url = "{{ action('chartcontroller@getDateUploadTest') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohokko: monthYears
        },
        success: function(dataResult){
            renderInstaller7(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller7(data){
Highcharts.chart('installer7', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color:'#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Upload Route Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c'
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            linecolor:'#24637c',
            fillcolor:'#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#638199',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}


//data Video Test

function getInstaller8(monthYears) {
    url = "{{ action('chartcontroller@getDateVideoTest') }}";
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        data: {
            molohokko: monthYears
        },
        success: function(dataResult){
            renderInstaller8(dataResult);
        },
        error: function(result,status,xhr){
            // Do something when get data result from HomeController@summaryTicket is error
            console.log('Installer get data is error');
        }
    });
}

function renderInstaller8(data){
Highcharts.chart('installer8', {
    chart: {
        type: 'line',
        zoomType: 'x',
        backgroundColor:'#c9e6f4',
        events: {
            load: function(event) {
              var total = 0;
                for(var i=0, len=this.series[0].yData.length; i<len; i++){
                    total += this.series[0].yData[i];
                }
              var text = this.renderer.text(
                'Total Per-bulan: ' + total,
                this.plotLeft,
                this.plotTop - 10
            ).attr({
                zIndex: 5,
            })
            .css({
                color:'#24637c',
                fontSize: '20px'
                })
            .add()
            }
              }
        },
    navigation: {
                buttonOptions: 
                {
                    enabled: false
                }
            },
    title: {
        text: 'Data Video Route Test September 2017 <br> Total Keseluruhan: ' + data.total,  //harus mengganti january dan 2017 menjadi dinamis mengikuti button filter diatas
        style: {
            color:'#24637c'
        },
    },
    subtitle: {
        text: null
    },
  credits: {
            enabled: true,
            position: {
                align: 'right',
                x: -10,
                verticalAlign: 'bottom',
                y: -5
            },
            href: "",
            text: ""
        },
    xAxis: {
        categories: data.categories,
        lineColor:'#24637c',
        tickColor:'#24637c',
        //min: 0,
        //max: 9,
        labels: {
            style:{
                color:'#24637c'
            }
        },
    },
    yAxis: {
        min: 0,
        gridLineColor: '#24637c',
        labels:{
            style:{
                color:'#24637c'
            },
        },
        title: {
            text: null
        }
    },
    plotOptions: {
        series: {
            linecolor:'#24637c',
            fillcolor:'#24637c'
        },
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    
    series: [{
        name: 'total',
        showInLegend: false,
        color:'#638199',
        data: data.dataSeries
    }],
    marker: {
                              lineWidth: 2,
                              lineColor: 'black',
                              fillColor: 'black',           
                              radius: 3,
                          },    
    // scrollbar: {
    //             enabled: true,
    //             barBackgroundColor: 'gray',
    //             barBorderRadius: 7,
    //             barBorderWidth: 0,
    //             buttonBackgroundColor: 'gray',
    //             buttonBorderWidth: 0,
    //             buttonArrowColor: 'yellow',
    //             buttonBorderRadius: 7,
    //             rifleColor: 'yellow',
    //             trackBackgroundColor: 'white',
    //             trackBorderWidth: 1,
    //             trackBorderColor: 'silver',
    //             trackBorderRadius: 7
    //         }
});
}

</script>
@stop