<html>
  <style>
    .footer {
  bottom: 0;
  width: 100%;
  /* Set the fixed height of the footer here */
  height: 60px;
  line-height: 60px; /* Vertically center the text there */
  background-color: #239dd2;
}
    </style>
<head>
<nav class="navbar navbar-default" style="background-color:#239dd2;">
  <div class="container-fluid">
    <div class="navbar-header">
    <img src="img/sv_logo_white.png" width="55%" padding="10px">
    </div>
  </div>
</nav>
</head>
<body>
@yield('content')
</body>
<br>
<footer class="footer">
<div class="container">
  <span class="text-muted" style="color:#fff; text-align:center;">&copy; 2017 Immobi Solusi Prima
</span>
</div>
</footer>

</html>